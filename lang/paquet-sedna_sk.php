<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-sedna?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'sedna_description' => 'Sedna slúži na kumulovanie informácií zo zdroja RSS (a ATOM a i.) zo syndikovaných stránok SPIPu. Jej vzhľad bol inšpirovaný programom Safari RSS.

Jej cieľom je stále mať jednoduchý kód a rýchlo sa spúšťať. A trochu aj estetický vzhľad.',
	'sedna_slogan' => 'Kumulovanie RSS'
);
