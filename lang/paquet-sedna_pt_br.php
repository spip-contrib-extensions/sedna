<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-sedna?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'sedna_description' => 'Sedna é um agregador de fluxo RSS (et ATOM etc) baseado nos sites sindicados do SPIP. Ele se inspira vagamente no «Safari RSS» para a apresentação.

Seu objetivo é ser e permanecer simples de código e rápido para carregar. Esteticamente, um pouco, também.',
	'sedna_slogan' => 'Agregador RSS'
);
