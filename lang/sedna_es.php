<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/sedna?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Mostrar los resúmenes',
	'afficher_sources' => 'Mostrar la fuente',
	'annee' => 'Año',
	'articles_recents_court' => 'Artículos recientes',

	// C
	'connexion' => 'Conexión',

	// D
	'deconnexion' => 'Desconexión',
	'derniere_syndication' => 'La última sindicación de este sitio se efectuó',
	'deuxjours' => 'Dos días',

	// L
	'liens' => '1 artículo',
	'liens_pluriel' => '@nb@ artículos',

	// M
	'masquer_resume' => 'Enmascarar los resúmenes',
	'masquer_sources' => 'Enmascarar esta fuente',
	'mois' => 'Mes',

	// P
	'pas_articles' => 'Sin artículos durante este periodo',
	'pas_synchro' => 'No sincronizar',
	'preferences' => 'Preferencias',
	'probleme_de_syndication' => 'hay un problema con la sindicación',

	// S
	'semaine' => 'Semana',
	'sources' => 'Fuentes',
	'synchro' => 'Sincronizar',
	'synchro_titre' => 'Grabar en el sitio la lista de los artículos leídos',
	'syndication_ajour' => 'Actualizar ahora',
	'syndication_fait' => 'Efectuada la sindicación',

	// T
	'toutes' => 'Todas'
);
