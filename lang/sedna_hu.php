<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/sedna?lang_cible=hu
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Összefoglalások megjelenítése',
	'afficher_sources' => 'Forrás megjelenítése',
	'annee' => 'Év',
	'articles_recents_court' => 'Újabb cikkek',

	// C
	'connexion' => 'Csatlakozás',

	// D
	'deconnexion' => 'Kijelentkezés',
	'derniere_syndication' => 'E honlap legújabb szindikálása megtörtént',
	'deuxjours' => 'Két nap',

	// L
	'liens' => 'cikk', # MODIF
	'liens_pluriel' => 'cikk', # MODIF

	// M
	'masquer_resume' => 'Összefoglalások rejtése',
	'masquer_sources' => 'Forrás rejtése',
	'mois' => 'Hónap',

	// P
	'pas_articles' => 'Nincs cikk abban az időszakban !',
	'pas_synchro' => 'Nincs szinkronizálás',
	'preferences' => 'Beállítások',
	'probleme_de_syndication' => 'szindikálási hiba',

	// S
	'semaine' => 'Hét',
	'sources' => 'Források',
	'synchro' => 'Szinkronizálás',
	'synchro_titre' => 'Menteni a honlapon az olvasott cikkek listáját',
	'syndication_ajour' => 'Frissíteni most',
	'syndication_fait' => 'Szindikálás megtörtént',

	// T
	'toutes' => 'Mindegyik'
);
