<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/sedna?lang_cible=km
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'បង្ហាញ អត្ថបទសង្ខេប',
	'afficher_sources' => 'បង្ហាញ ប្រភពនេះ',
	'annee' => 'ឆ្នាំ',
	'articles_recents_court' => 'អត្ថបទថ្មីៗ',

	// C
	'connexion' => 'បញ្ជាប់',

	// D
	'deconnexion' => 'ពិនិត្យចេញ',
	'deuxjours' => 'ពីរថ្ងៃ',

	// L
	'liens' => 'អត្ថបទ', # MODIF
	'liens_pluriel' => 'អត្ថបទ នានា', # MODIF

	// M
	'masquer_resume' => 'បិទបាំង អត្ថបទសង្ខេប',
	'masquer_sources' => 'បិទបាំង ប្រភពនេះ',
	'mois' => 'ខែ',

	// P
	'preferences' => 'ចំណូលចិត្ត',

	// S
	'semaine' => 'សប្តាហ៍',
	'sources' => 'ប្រភព',

	// T
	'toutes' => 'ទាំងអស់'
);
