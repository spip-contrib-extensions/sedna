<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/sedna?lang_cible=gl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Amosar os resumos',
	'afficher_sources' => 'Amosar esta fonte',
	'annee' => 'Ano',
	'articles_recents_court' => 'Artigos recentes',

	// C
	'connexion' => 'Conexión',

	// D
	'deconnexion' => 'Desconexión',
	'derniere_syndication' => 'A última sindicación deste web foi efectuada',
	'deuxjours' => 'Dous días',

	// L
	'liens' => 'artigo', # MODIF
	'liens_pluriel' => 'artigos', # MODIF

	// M
	'masquer_resume' => 'Ocultar os resumos',
	'masquer_sources' => 'Ocultar esta fonte',
	'mois' => 'Mes',

	// P
	'pas_articles' => 'Non hai ningún artigo deste período !',
	'pas_synchro' => 'Non sincronizar',
	'preferences' => 'Preferencias',
	'probleme_de_syndication' => 'Problema de sindicación',

	// S
	'semaine' => 'Semana',
	'sources' => 'Fontes',
	'synchro' => 'Sincronizar',
	'synchro_titre' => 'Rexistrar no web a lista dos artigos lidos',
	'syndication_ajour' => 'Actualizar agora',
	'syndication_fait' => 'A sindicación foi efectuada',

	// T
	'toutes' => 'Todos'
);
