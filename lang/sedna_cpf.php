<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/sedna?lang_cible=cpf
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Afis bann rézimé ',
	'afficher_sources' => 'Afis sours-la',
	'annee' => 'Anné',
	'articles_recents_court' => 'Bann zatik pli frè',

	// C
	'connexion' => 'koneksyon',

	// D
	'deconnexion' => 'Dekoneksyon',
	'derniere_syndication' => 'Dernyè sendikasyon la fin fini : ',
	'deuxjours' => 'Dé zour',

	// L
	'liens' => 'atik', # MODIF
	'liens_pluriel' => 'bann zatik', # MODIF

	// M
	'masquer_resume' => 'Kasé tout rézimé',
	'masquer_sources' => 'Kasé sours-la',
	'mois' => 'Mwa',

	// P
	'pas_articles' => 'Ni trouv pa okin atik pou périod-la !',
	'pas_synchro' => 'senkoniz pa li la',
	'preferences' => 'Kosa ou èm myé',
	'probleme_de_syndication' => 'larlik èk lasendikasyon',

	// S
	'semaine' => 'Semèn',
	'sources' => 'Tout bann sours',
	'synchro' => 'Senkoniz',
	'synchro_titre' => 'Enrozistr andan sit-la lo lis tout bann zatik domoun la plis lu',
	'syndication_ajour' => 'Rafresi astèr',
	'syndication_fait' => 'Sendikasyon la fin fini',

	// T
	'toutes' => 'Tout'
);
