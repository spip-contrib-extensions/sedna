<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-sedna?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'sedna_description' => 'Sedna is an aggregator (RSS and ATOM etc.) based on SPIP’s syndicated sites. It is based loosely on "Safari RSS" for presentation.

Its purpose is to be and remain simple in its code, and quick to load. Aesthetics a bit, too.',
	'sedna_slogan' => 'RSS aggregator'
);
