<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/sedna?lang_cible=ro
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aff_resume' => 'Afişaţi rezumatele',
	'afficher_sources' => 'Afişaţi această sursă',
	'annee' => 'Anul',
	'articles_recents_court' => 'Articole recente',

	// C
	'connexion' => 'Conexiune',

	// D
	'deconnexion' => 'Deconexiune',
	'derniere_syndication' => 'Ultima sindicalizare a acestui site efectuată',
	'deuxjours' => 'Două zile',

	// L
	'liens' => 'articol', # MODIF
	'liens_pluriel' => 'articole', # MODIF

	// M
	'masquer_resume' => 'Mascaţi rezumatele',
	'masquer_sources' => 'Mascaţi această sursă',
	'mois' => 'Luni',

	// P
	'pas_articles' => 'Nici un articol în această perioadă !',
	'pas_synchro' => 'Nu sincronizaţi',
	'preferences' => 'Preferinţe',
	'probleme_de_syndication' => 'problemă de sindicalizare',

	// S
	'semaine' => 'Săptămână',
	'sources' => 'Surse',
	'synchro' => 'Sincronizaţi',
	'synchro_titre' => 'Înregistraţi pe site lista articolelor citite',
	'syndication_ajour' => 'Aduceţi la zi acum',
	'syndication_fait' => 'Sindicalizare efectuată',

	// T
	'toutes' => 'Toate'
);
